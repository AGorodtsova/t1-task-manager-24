package ru.t1.gorodtsova.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User id not found...");
    }

}
