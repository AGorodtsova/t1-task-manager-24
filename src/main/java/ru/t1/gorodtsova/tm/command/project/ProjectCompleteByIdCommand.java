package ru.t1.gorodtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Complete project by id";

    @NotNull
    private final String NAME = "project-complete-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(getUserId(), id, Status.COMPLETED);
    }

}
